package ru.tsc.anaumova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.anaumova.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.anaumova.tm.dto.model.TaskDto;
import ru.tsc.anaumova.tm.enumerated.Sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Repository
public class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDto> implements ITaskDtoRepository {

    @NotNull
    @Override
    public List<TaskDto> findAll() {
        @NotNull final String jpql = "SELECT m FROM TaskDto m";
        return entityManager.createQuery(jpql, TaskDto.class).getResultList();
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@NotNull String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM TaskDto m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@NotNull String userId, @NotNull Sort sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM TaskDto m WHERE m.userId = :userId ORDER BY m."
                + getSortType(sort.getComparator());
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@NotNull String userId, @NotNull Comparator<TaskDto> comparator) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM TaskDto m WHERE m.userId = :userId ORDER BY m." + getSortType(comparator);
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDto findOneById(@NotNull String id) {
        return entityManager.find(TaskDto.class, id);
    }

    @Nullable
    @Override
    public TaskDto findOneById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM TaskDto m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull String id) {
        Optional<TaskDto> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        Optional<TaskDto> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM TaskDto";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void clear(@NotNull String userId) {
        if (userId.isEmpty()) return;
        @NotNull final String jpql = "DELETE FROM TaskDto m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public long getCount() {
        @NotNull final String jpql = "SELECT COUNT(e) FROM TaskDto e";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public long getCount(@NotNull String userId) {
        @NotNull final String jpql = "SELECT COUNT(e) FROM TaskDto e WHERE e.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String id) {
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM TaskDto m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty() || id.isEmpty()) return false;
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM TaskDto m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @NotNull
    @Override
    public List<TaskDto> findAllByProjectId(@NotNull String userId, @NotNull String projectId) {
        if (userId.isEmpty() || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM TaskDto m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeTasksByProjectId(String projectId) {
        @NotNull final String jpql = "DELETE FROM TaskDto m WHERE m.userId = :userId AND m.projectId = :projectId";
        entityManager.createQuery(jpql)
                .setParameter("userId", TaskDto.class)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}