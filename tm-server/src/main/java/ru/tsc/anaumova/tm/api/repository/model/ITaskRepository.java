package ru.tsc.anaumova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTasksByProjectId(@NotNull String userId, @NotNull String projectId);

}
