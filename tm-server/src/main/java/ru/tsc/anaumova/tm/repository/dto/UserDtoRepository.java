package ru.tsc.anaumova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.anaumova.tm.api.repository.dto.IUserDtoRepository;
import ru.tsc.anaumova.tm.dto.model.UserDto;

import java.util.List;
import java.util.Optional;

@Repository
public class UserDtoRepository extends AbstractDtoRepository<UserDto> implements IUserDtoRepository {

    @NotNull
    @Override
    public List<UserDto> findAll() {
        @NotNull final String jpql = "SELECT m FROM UserDto m";
        return entityManager.createQuery(jpql, UserDto.class).getResultList();
    }

    @Nullable
    @Override
    public UserDto findOneById(@NotNull String id) {
        return entityManager.find(UserDto.class, id);
    }

    @Override
    public void removeById(@NotNull String id) {
        Optional<UserDto> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM UserDto";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public long getCount() {
        @NotNull final String jpql = "SELECT COUNT(e) FROM UserDto e";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String id) {
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM UserDto m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public UserDto findOneByLogin(@NotNull String login) {
        @NotNull final String jpql = "SELECT m FROM UserDto m WHERE m.login = :login";
        return entityManager.createQuery(jpql, UserDto.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDto findOneByEmail(@NotNull String email) {
        @NotNull final String jpql = "SELECT m FROM UserDto m WHERE m.email = :email";
        return entityManager.createQuery(jpql, UserDto.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

}