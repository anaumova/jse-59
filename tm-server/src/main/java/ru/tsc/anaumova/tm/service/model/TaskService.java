package ru.tsc.anaumova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.anaumova.tm.api.repository.model.ITaskRepository;
import ru.tsc.anaumova.tm.api.service.model.ITaskService;
import ru.tsc.anaumova.tm.exception.field.EmptyDescriptionException;
import ru.tsc.anaumova.tm.exception.field.EmptyIdException;
import ru.tsc.anaumova.tm.exception.field.EmptyNameException;
import ru.tsc.anaumova.tm.exception.field.EmptyUserIdException;
import ru.tsc.anaumova.tm.model.Task;

import java.util.Date;
import java.util.List;

@Service
public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Override
    protected ITaskRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @Transactional
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setUser(getUserRepository().findOneById(userId));
        @NotNull final ITaskRepository repository = getRepository();
        repository.add(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(getUserRepository().findOneById(userId));
        @NotNull final ITaskRepository repository = getRepository();
        repository.add(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(getUserRepository().findOneById(userId));
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        @NotNull final ITaskRepository repository = getRepository();
        repository.add(task);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository repository = getRepository();
        return repository.findAllByProjectId(userId, projectId);
    }

}